
# Introduction

Long ago, the World lived in harmony.
The ancient ones had settled in an age of peace and prosperity.
They had shaped the earth to their liking, they looked over the woods, they subdued the heat of the mountains and deserts, they had harnessed the wind and even tamed the sea.
They did this by their discovery and knowledge of a craft they simply called ‘alchemy’.
Through rituals, rites, experiments and hard work; they understood that everything has a purpose and a place and with time, they understood the fabric of reality of their existence but not without a price.
 To shape things in their vision, they would always need a catalyst, they must harness and use the energy of one thing if they must change another.
It was known to them as the `Great Bargaining'.

As generations spawned, this ability became easier and more attuned with the inhabitants of this World.
The descendants of the Ancient Ones had now all but split off into different yet peaceful factions.
Those who could tame the sea, found themselves becoming one with it, known as the Meer.
Excellent swimmers and masters of Water.
Those who endured the blistering heat, found their skin become scaled as they were known as the Lizard race, masters of Fire.
Those in the Sky took flight and became known as the Pari, masters of Air.
The Elves, stayed in their woodland homes, looking over the creatures and animals in harmony, as they became masters of Wood.
The cavernous underground, stunted the growth of the Dwarves, as they became the masters of Earth.
Between all five, peace continued from generations to generations...
however, not all agreed with the laws of the bargain.  Some wanted more power, that more progress could be made.   That they could become masters of life itself.

First were the Golems, working creatures with no will or thought of their own.
Great power was used to create them, sometimes too great.
As even masters of their craft found themselves that the bargain was their own life in order for such powerful Alchemy to work.
They would obey and they would work, but they knew not how to do anything without strict commands.

More and more, life was to be created, and then it was found.
The secret of all five elements brought together unlocked a sixth.
The power of Metal.
One known simply as the 'Mad Creator' spent a lifetime studying and unlocking the secrets of all five elements.
Not content with a master of one.
Some say they were an Elvish witch, driven insane by loneliness.
Others tell of a Meer King, who looked to expand his empire, a deranged Dwarf of the deep where the darkness took hold of his mind and compelled him.
A Pari who wished to fly higher than all the rest, driven by ambition.
The Lizard Queen who wanted to secure her reign for the rest of time...the factions had all their own tales about how it came to be, but none knows the truth.  All that is known, is the `Mad Creator' gave life to Metal, and with it a will of its own.

And so, the first Machine was made and it was alone, it wondered where more of its kind existed.
Thus, it learned and became the first master of Metal.
It made more machines, who in turn made more and more.
Their will was insatiable and War broke out.

The various elemental factions of the World closed their borders and their homes, they sought only to protect themselves and as the Machines walked upon the World, the factions fought for more control over safer areas.
They fought one another and even within their own homes, leaders, squabbled and argued, there was no right answer to deal with the Machines and with no right answer, there were riots and revolutions.

Now in the present day, there are those who still wield great Alchemical power.
Those who wish to stop the Machine onslaught before it devours the world, and those who would wish to see the elements in harmony again.
They are the ones who remember the Ancient Ones, they remember the ways of the `Great Bargaining' and they exist unaware of each other all across the land.

They are the Alchemists, and this, is their Odyssey...

