I am uncertain who will
read this nor if it will arrive in safe hands. All I can do is pray to the Ancient Ones and hope, hope that this
reaches the eyes of the one I intended. The history of our World is long and blessed with such gifts but there
is also a dark passage. A shadow that
has appeared and crept over our landscape, seeking to cover everything in its
path.

You must be aware of the
threat they pose, to our way of life, to the very fabric of the heart of this
World. If you are not, allow me to make
you aware. For you see I too am like
you, yes, there are others. Not many but
not uncommon either. The beings of this
place, they are masters of the elements that they are attuned too. Have you ever seen the Pari and how the grace
they air? Or how the Meer both swim and
captain ships with such ease? The
Lizardfolk are great weaponsmiths for their ways of fire are second to
none! And if you ever wanted someone to
help with brick and stone, look no further than the Dwarves and their
craft. They are the finest stonemasons
you’ll ever see. Finally, the Elves…
what music and beauty they have and wait until you see how the marvels of their
woods, the greatest bows ever made and they have such a way with animals. I’ve never seen the likes of wild beasts act
domesticated when around them!  

I digress however, my point stands while those races may be talented and
masters of their own realms… you have found yourself apart. Just as I was born here, so too were you and
thus you will belong to one of those tribes and yet… you are disconnected. What is a Dwarf meant to do when he discovers
he can shape water? What does an Elf do
when they take flight like the birds they keep? Yes, they are attuned to one element but some of us have the gift of
more.

You know your history as
well as I do. The time of the ‘Mad
Creator’, when the first Machine came into existence. Who knows what they were thinking. Perhaps an age of prosperity? Maybe they thought it was a breakthrough in
our lives, the next step so to speak. Maybe the stories of them seeking power for vanity are true. Or possibly, they were just indeed mad and
who knows what thoughts compelled them. All we know, is we now share this World with another. The Machines.

They call it a War… the
Machine War. Hah! Can you believe it? For every metal demon that we strike down,
two more are quickly created to take its place. They need fuel you see, they do not live as we live. I have heard tales of ones from long ago that
still prowl the lands to these days. All
they require to function is the water and soil that they consume in abundant
amounts but it is not for sustenance, no. It is so they are ‘powered’ and can create more of their like.  

Do you see? They create, consume and
create. There is no tending to the land,
no prosperity. Only them and their need
to continue. Eventually if they are not
stopped, the entire World will be as desolate and barren as their own lands. They are a disease, creeping and edging
towards total destruction of all life until only they remain.  

This… this is where I hope you come in. I spoke earlier of your feeling of disconnection… what if I was to tell
you that whatever talents or powers you might wield, that you can find more? We are a hidden kind and only seek out those
who have the potential. As while the
tribes and races have hidden away and cowered behind their borders, someone
must take a stand and that is us. We
have the power of all elements, untrained perhaps, and it will take work. A lot of work. But the potential is there. Is this a gift from the Ancients? The by-product of the Mad Creator? The World seeking to repair itself and thus
its gifts are bestowed to us? Who knows…
all that I do is we have gathered and we invite you to join us.  

I hope this reaches you in good stead and that you will be with us soon. Forgive me for not using names, I cannot
reveal myself but all will come clear in person if you can make it and if it is
truly you whom this letter has reached. If it is indeed you, then you will know where the Moon shines twice and
meets the peak. You fill find us there.

May the Ancients bless
you,  

One of many,  
An Alchemist
