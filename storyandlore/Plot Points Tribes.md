<style>
</style>

PLOT POINT LIST

The Meer – Alish,
rightful heir to the throne has abandoned his responsibilities, wishing to seek
more than the Meer Underwater cities. This has caused a civil war, with another Meer claiming that THEY are in
fact the rightful ruler and that they should be entrusted to expand the Meer
empire as the protection to stop the Machines whereas Alish wishes to make
peace and cooperate with others.

The Elves – Long living
and practically immortal, the Elves do not worry and do not take the threat of
the Machine’s seriously. Feeling like it
would be just another passing matter of the fabric of time, until they find a
horde upon their doorstep with one of their settlements massacred. It is only then they take it seriously but at
this point, it might be too late.  

The Dwarves – Stonemasons and craftsmen, they are stubborn and see the Machines
as some sort of marvel, wishing to study and work with them to a compromise
rather than see them destroyed. However
the Machine’s themselves obey only each other.

The Pari – The Pari are
isolated due to their power of flight.
They see themselves as above the War
that takes place on the World below and it will take a great event to make them
see that if the World falls, so does their way of life as not everything can be
gained from the sky.  

The Lizardfolk – They are the main forces who have held fast against the
Machines, a proud and battle hardened race. They have little trust of outsiders and less so for any who flee or who
refuse to fight, thus they see themselves as sole protectors, driving headlong
into a merciless and futile task.
