# How the Races Live

## Air – Pari & Birds

Pari live in the Iron Oak/Redwood trees which are giant
trees which cannot be cut down however the trees at the top have leafy tops and
the wood is malleable. They have a way
of living in which they make the wood see-through thus they can see the outside
World. They control air, thus the
buildings they construct are all fully aerodynamic. The older they become, the more birdlike they
get which means that the eldest Pari are akin to giant birds of prey.

Demeanour is they keep to themselves; they do small amounts
of trade.

## Water – Meer & Creatures of the Deep

They live in constructed coral structures, made and crafted
by the most skilled of the Meer builders. They have mastered the currents and though can have tunnels within their
settlements which have updrafts going one way or the next to get from area to
area with great speed and haste. The
Meer are an Ocean race and thus although they share some genetics with one
another, they can have vastly different appearances and due to this, some may
never require sleep, others may only sleep for short periods of time while
others may require a full rest.  

They do small bits of trade with other races, but due to their Imperial nature,
they are quite distrusting of outsiders unless they can prove themselves in some
way. Once you have gained the trust of a
Meer, you have gained an ally for life. Should you ever betray one however, vengeance will be never ending, such
is their wrath at being wronged.

## Wood – Elves, Fae, Wendigos & Woodland Creatures

Live in the Woodlands, harmonious with Nature and balance
with life. Once they might have been
curious, nice and even generous. Time has taught them otherwise, centuries of
their homelands being destroyed has seen them angered and now, there is no more
niceness within them. They are the full
wrath of Nature embodied. Their homes
are within trees themselves, sprouting roots shaped without harm to provide the
space and tools for whatever the fey need.

Demeanour is they trust no other outsiders as their
resources are always taken without asking and are always stolen. Stepping into the woods, will more than
likely see you killed if the Fae live there.

## Fire – Ethala

Nomadic race, fire brings the warmth of life. Destruction breeds creation when used
correctly. Overgrown forests find the
Ethala coming to burn down the excess so that life can breathe. Those in the mountains seek them out for
warmth. They are seen as possibly the
first of all races and thus on land are welcome into all homes, even the Fae
see them as one with Nature and an extension of it. They are an oral culture who move, weaving
stories from town to town and continuing on their journey. What their true purpose is, no one knows save
for the Ethala themselves.  

In terms of their living quarters, their scales are their comfort, the ground
is their bedding. Masters of fire and
due to the nature of their own bodies, they easily find sleep whenever they
need rest though they will always take offer of a cosy room whenever offered.

## Earth – Talpa

A curious race who digs deep underground and make their
homes both there and within the stone mountains. A curious race who freely trade for comforts
and things they enjoy, as the minerals of the earth and soil are easy for them
to obtain but very difficult for other races. Thus, others are happy to see a Talpa and freely trade things like
pillows, cloth and feather bedding in return for what Talpa’s see as plain
dirt. They travel a little but mostly
reside to their cosy homes in which they are familiar with and where they have
made a family. A colony race where most
Talpa will know each other, if not by name, then definitely by smell as all
Talpa families have a unique scent which only their noses can discern.

## Metal – Cogwork, Machines & Man

The varied and most populated species, the species of
man. They live a short lifespan compared
to other races and thus they have a fervour to make the most of their lives,
unable to use the elements like the other races, they turn to their technology
and have learned the ways of the machine, for better or for worse. Many humans try to work peacefully with other
races however other races see humans as selfish and greedy as they feel that
they take without asking and give nothing back, caring only to better their own
lives with no thought to the consequences they bring to others. Though this isn’t helped by the fact none
have told the humans the effects of their actions, and opt for killing them
outright instead.

They live in populated cities that they built by
hand though there are a few among them who have taken the time to understand
the World and their place in it and for that, their studies have awoken the
power of Alchemy within them. For all
humans have the greatest minds for study and potential, if only most of them
were aware of it.
